import globals from "./globals.js";
//importamos loadAssests
import {initHTMLelements, loadAssests, initVars} from "./initialize.js;";

import update from "./gamelogic.js,";
import render from "./gamerender.js";

/////////////////////////////////////////////
//GAME INIT
/////////////////////////////////////////////

window.onload= init;

function init ()
{
    //inicializamos los elementos hTML: CANVAS, CONTEXT, caja de texto de pruebas
    initHTMLelements();

    //cargamos todos los activos: tilemaps, images, sounds
    loadAssests();
    //inicializacion de variables del juego
    initVars();

    //start the firts frame request
    window.webkitRequestAnimationFrame(gameloop);

}

