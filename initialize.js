import globals from "./globals";

//carga de activos : Tilemaps, images, sounds
function loadAssets()
{
    //load the tileset image
    globals.tileSet = new Image ();
    globals.tileSet.addeventlistener("load", loadhandler, false);
    globals. tileSet.src = "./images/player.png"; //ojo de la ruta es la relativa al Html, no al JS
    globals.assetstoload.push(globals.tileSet);
}

//funcion que se llama cada vez qye se carga un activo
function loadhandler()
{
    globals.assetsloaded++;
    //UNA VEZ SE HAN CARGADO TODOS LOS ARCIHVOS 
    if (globals.assetsloaded===globals.assetstoload.length)
    {
        //remove the load event listener
        globals.tileSet.removeeventlistener("load", loadhandler, false);

        console.log ("assets finished loading");
        //start the game
        globals.gameState= Game.PLAYING;
    }
}

export {
    initHTMLelements,
    initVars,
    loadAssets
}

