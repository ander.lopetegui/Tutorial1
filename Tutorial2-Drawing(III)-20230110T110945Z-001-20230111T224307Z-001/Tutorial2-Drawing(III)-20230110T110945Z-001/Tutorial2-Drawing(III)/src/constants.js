//Constants

//Estados del juego
export const Game = {
    INVALID: -1,
    LOADING:  0,
    PLAYING:  1,
    OVER:     2
};

//Velocidad del juego
export const FPS = 30;

//Identificador de tipo de sprite (ID)
export const SpriteID = {
    PLAYER: 0,
    JERINGUILLA: 1,
    SLIME: 2,
    COIN: 3,
    DEMON: 4
}

//identificador de estado de sprite (direccion)
export const State = {
    // Estados PLAYER
    UP: 3,
    LEFT: 1,
    DOWN: 0,
    RIGHT: 2,
    STILL_UP: 11,
    STILL_LEFT: 9,
    STILL_DOWN: 8,
    STILL_RIGHT: 10,
   

    //Estados PIRATE
    LEFT_2: 0,
    RIGHT_2: 1,

    //Estados JOKER, KNIGHT
    STILL: 0
}

//diferentes tileSets
export const Tile = {
    SIZE_64: 0,
    SIZE_32: 1
}

//id de bloque del mapa
export const Block = {
    EMPTY:      0,
    VINES:      1,
    BROWN_1:    2,
    BROWN_2:    3,
    DARK_1:     4,
    GRAY:       5,
    CRISTAL_1:  6,
    CRISTAL_2:  7,
    PARED: 8,
    PARED_1: 9,
    PARED_2: 10

}

//KEYBOARD
export const Key = {
    UP: 38,
    DOWN: 40,
    RIGHT: 39,
    LEFT: 37,
    SPACE: 32, 
}