export default class Frames
{
    constructor(framesPerState, speed =1)
    {
        this.framesPerState = framesPerState;     //Número de frames por estados de animación
        this.frameCounter  = 0;
        this.speed     = speed;
        this.frameChangeCounter= 0;           // contador de frames
    }
}
