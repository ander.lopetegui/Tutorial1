import globals from "./globals.js";
import { Game, State, SpriteID } from "./constants.js";
import { initjeringuilla } from "./initialize.js";


export default function update()
{
    //Change what the game is doing based on the game state
    switch(globals.gameState)
    {
        case Game.LOADING:
            console.log("Loading assets...");
            break;
        
        case Game.PLAYING:
            playGame();
            break;
        
        default:
            console.error("Error: Game State invalid");
    }
}

//Por último, desde gameLogic.js, podremos cambiar los atributos de nuestro personaje, a través de la función updatePlayer(). 

//funcion que actualiza el personaje
function updatePlayer(sprite)
{
    readKeyboardAndAssignState(sprite);

    switch ( sprite.state)
    {
        case State.UP:
        sprite.physics.vx=0
        sprite.physics.vy= -sprite.physics.vLimit;
        break;
        
        case State.DOWN:
            sprite.physics.vx=0
            sprite.physics.vy= sprite.physics.vLimit;
            break;
        
        case State.RIGHT:
            sprite.physics.vx= sprite.physics.vLimit;
            sprite.physics.vy= 0;
            break;
        
        case State.LEFT:
            sprite.physics.vx= -sprite.physics.vLimit;
                sprite.physics.vy= 0;
                break;
            
                    
        default:
            sprite.physics.vx=0;
            sprite.physics.vy= 0;
        
    }
    if(globals.action.moveSpace)
    {
        initjeringuilla(sprite);
        console.log("entrar");
    }

    sprite.xPos += sprite.physics.vx * globals.deltaTime;
    sprite.yPos += sprite.physics.vy * globals.deltaTime;

    updateAnimationFrame (sprite);

}

function updateAnimationFrame(sprite) 
{
    console.log (sprite.frames.frameChangeCounter);
    switch(sprite.state)
    {
        case State.STILL_UP:
        case State.STILL_LEFT:
        case State.STILL_DOWN:
        case State.STILL_RIGHT:
            sprite.frames.frameCounter = 0;
            sprite.frames.frameChangeCounter = 0;
            break;
        default:
            
        sprite.frames.frameChangeCounter++;

        if (sprite.frames.frameChangeCounter === sprite.frames.speed)
        {
            sprite.frames.frameCounter++;
            sprite.frames.frameChangeCounter = 0;

        }

        if (sprite.frames.frameCounter === sprite.frames.framesPerState)
        {
            sprite.frames.frameCounter = 0;
            
        }
    }
}

// updateSprite(): Función que accede al id del sprite y llama a la función que actualiza cada tipo de sprite.

// updateSprites(): Función que recorrerá el array sprites y llamará para cada uno de ellos a updateSprite().

function updateSlime(sprite)
{
    // aqui actualizaremos el estado de ls vriables del pirata
    switch (sprite.state)
    {
        case State.RIGHT_2:
        sprite.physics.vx = sprite.physics.vLimit;
        break;
        
        case State.LEFT_2:
        sprite.physics.vx = sprite.physics.vLimit;
        break;

        default:
            console.error("error:invalid")

    }
    sprite.xPos += sprite.physics.vx * globals.deltaTime;

    updateAnimationFrame(sprite);
}

function updateCoin(sprite)
{
    //Aqui actualizaremos el estado de las variables del player
   
    sprite.frames.frameChangeCounter++;

    if (sprite.frames.frameChangeCounter === sprite.frames.speed)
    {
        sprite.frames.frameCounter++;
        sprite.frames.frameChangeCounter = 0;

    }

    if (sprite.frames.frameCounter === sprite.frames.framesPerState)
    {
        sprite.frames.frameCounter = 0;
        
    }
}  


function updateLevelTime()
{
    //icrementra
    globals.levelTime.timeChangeCounter += globals.deltaTime;

    //si ha passado
    if (globals.levelTime.timeChangeCounter > globals.levelTime.timeChangeValue)
    {
        globals.levelTime.value--;

        //resetear
        globals.levelTime.timeChangeCounter = 0;
        
    }
}

function playGame()
{
    updateSprites();
    updateLevelTime();
}

function updateSprites()
{
    for (let i = 0; i < globals.sprites.length; ++i)
    {
        const sprite = globals.sprites[i];
        updateSprite(sprite);
    }
    
}


function updatejeringuilla(sprite)
{
    //Aqui actualizaremos el estado de las variables del player
    sprite.xPos = 20;
    sprite.yPos = 32;

    sprite.frames.frameCounter = 0;

    sprite.state = State.DOWN;
}



function updateDemon(sprite)
{
    //aqui actualizaremos el estado de ls vriables del pirata
    switch (sprite.state)
    {
        case State.RIGHT_2:
            sprite.physics.vx = sprite.physics.vLimit;
            break;
        
        case State.LEFT_2:
        sprite.physics.vx = sprite.physics.vLimit;
        break;

        default:
            console.error("error:invalid")

    }
    sprite.xPos += sprite.physics.vx * globals.deltaTime;
}

function readKeyboardAndAssignState(sprite)
{
    sprite.state = globals.action.moveLeft  ? State.LEFT:
                   globals.action.moveRight ? State.RIGHT:
                   globals.action.moveUp    ? State.UP:
                   globals.action.moveDown  ? State.DOWN:
                   sprite.state === State.LEFT  ? State.STILL_LEFT:
                   sprite.state === State.RIGHT ? State.STILL_RIGHT:
                   sprite.state === State.UP    ? State.STILL_UP:
                   sprite.state === State.DOWN  ? State.STILL_DOWN:
                   sprite.state;

}

function updateSprite(sprite)
{
    const type = sprite.id;
    switch (type)
    {
        //caso del jugador
        case SpriteID.PLAYER:
            updatePlayer(sprite)
            break;
        
        case SpriteID.DEMON:
            updateDemon(sprite)
            break;

        //case del pirata
        case SpriteID.COIN:
            updateCoin(sprite);
            break;

        //case del pirata
        case SpriteID.SLIME:
            updateSlime(sprite);
            break;

         //case del pirata
         case SpriteID.JERINGUILLA:
            updatejeringuilla(sprite);
            break;

        //caso del enemigo
        default:
            break;
    }
}

